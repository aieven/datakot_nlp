from nltk.stem.porter import PorterStemmer
from nltk.corpus import stopwords
import re
import pandas as pd

class Tokenizer():

    def __init__(self, POS_list={'pymorphy':['NOUN', 'ADJF', 'ADJS', 'COMP', 'VERB', 'INFN', 'PRTF', 'PRTS', 'GRND', 'NUMR'],
                                 'pymystem':['A', 'ANUM', 'COM', 'NUM', 'S', 'V']}):
        self._pos_list = POS_list
        self._porter = PorterStemmer()
        self._stopwords = stopwords.words('english')

    def tokenizer_nltk(self, text):
        '''разбивает текст на слова в нормальной форме пакет pymorph'''
        text = self._clear_other_symbols(text)
        list = []
        for word in text.split():
            if word not in self._stopwords:
                list.append(self._porter.stem(word))
        return list

    def _clear_other_symbols(self, text):
        '''очищает текст от знаков препинания'''
        text = re.sub('[\W]+', ' ', text.lower())
        return text

    def _delete_stop_words(self, list_words):
        '''удаляет незначимые части речи'''
        words = []
        for word in list_words:
            if len(self._define_POS(word)) > 1:
                words.append(word)
        return words
