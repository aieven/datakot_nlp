from __future__ import print_function
from time import time
import pandas as pd
import numpy as np
import operator
from Tokenizer import Tokenizer
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation
from sklearn.datasets import fetch_20newsgroups
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler

n_samples = 2000
n_features = 3000
n_topics = 20
n_top_words = 20

dataset = pd.read_csv("C:/Users/user/PycharmProjects/datacod_nlp/dict (5).csv", sep=';', encoding='ASCII')

dataset.columns = ['tag', 'tokens']
dataset['text'] = dataset['tokens'].apply(lambda x: x.replace('[', '').replace(']', '').replace("u'", '')
                                          .replace("'", '').replace(",", ''))

data_samples = dataset['text']

# Use tf (raw term count) features for LDA.
print("Extracting tf features for LDA...")
tf_vectorizer = TfidfVectorizer(max_features=n_features, binary=True)
tf = tf_vectorizer.fit_transform(data_samples)

lda = LatentDirichletAllocation(n_topics=20, max_iter=5,
                                learning_method='online',
                                learning_offset=50.,
                                random_state=0)
t0 = time()
lda.fit(tf)

tf_feature_names = tf_vectorizer.get_feature_names()

model = pd.DataFrame(lda.components_.transpose())

sc = StandardScaler()
data_scale = sc.fit_transform(model)

#predict
new_questions = """I have a dictionary of values read from two fields in a database: a string field and a numeric field. The string field is unique, so that is the key of the dictionary.

I can sort on the keys, but how can I sort based on the values?

Note: I have read Stack Overflow question How do I sort a list of dictionaries by values of the dictionary in Python? and probably could change my code to have a list of dictionaries, but since I do not really need a list of dictionaries I wanted to know if there is a simpler solution."""

question = tf_vectorizer.fit_transform([new_questions])
quest_lda_predict = lda.fit_transform(question)
predict_scale = sc.fit_transform(quest_lda_predict)

distances = []
for row in data_scale.transpose():
    distances.append(np.linalg.norm(row-predict_scale))

dataset['distances'] = len(distances)
