import unittest

from src.tokenizer import Tokenizer


class TestTokenizer(unittest.TestCase):
    tokenizer = Tokenizer()

    def test_short_string(self):
        text = "This is a python3.6 version."
        tokens = ["this", "python3.6", "version"]
        self.assertEqual(self.tokenizer.tokenize(text), tokens)

    def test_long_string(self):
        text = "These features were implemented in a python3.6, not java 7 version. " \
               "Other details about Python 3.6.1 see in docs."
        tokens = ["these", "featur", "implement", "python3.6", "java", "7", "version",
                  "other", "detail", "python", "3.6.1", "see", "doc"]
        self.assertEqual(self.tokenizer.tokenize(text), tokens)

    def test_string_with_commas_and_sharps(self):
        text = "These features were implemented in a python3,6, c++, .net, a_n, a-n1, c# and C #."
        tokens = ["these", "featur", "implement", "python3.6", "c++", ".net", "a_n", "a-n1", "c#", "c#"]
        self.assertEqual(self.tokenizer.tokenize(text), tokens)

if __name__ == '__main__':
    unittest.main()